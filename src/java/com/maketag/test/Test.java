/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maketag.test;
import com.maketag.*;
import com.maketag.VAST.Ad;
import com.maketag.VAST.Ad.Wrapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
/**
 *
 * @author Eugenio
 */
public class Test {
 
 public static void main(String[] args){
     /*
      <VAST> <Ad> <Wrapper> …
<VASTAdTagURI>
http://SecondaryAdServer.vast.tag
</VASTAdTagURI>
…</Wrapper> </Ad> </VAST>
      */
     ObjectFactory factory=new ObjectFactory();
     VAST v=factory.createVAST();
     Ad ad=factory.createVASTAd();
     Wrapper w=factory.createVASTAdWrapper();
     w.setVASTAdTagURI("http://SecondaryAdServer.vast.tag");
     ad.setWrapper(w);
     v.getAd().add(ad);
     Wrapper w2=factory.createVASTAdWrapper();
     w2.setVASTAdTagURI("http://www.gmail.com");
     Ad ad2=factory.createVASTAd();
     ad2.setWrapper(w2);
     v.getAd().add(ad2);
     
     JAXBContext jc;
        try {
            jc = JAXBContext.newInstance("com.maketag");
            //Crear clasificador
            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            //Clasificar objeto en archivo.
            m.marshal(v, System.out);
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }

 } 
    
}
