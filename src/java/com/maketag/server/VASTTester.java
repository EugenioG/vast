/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maketag.server;

import com.maketag.ObjectFactory;
import com.maketag.VAST;
import com.maketag.VAST.Ad;
import com.maketag.VAST.Ad.Wrapper;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

/**
 *
 * @author Eugenio
 */
@WebServlet(name = "VASTTester", urlPatterns = {"/VASTTester"})
public class VASTTester extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
       try {
             ObjectFactory factory=new ObjectFactory();
             VAST v=factory.createVAST();
             Ad ad=factory.createVASTAd();
             Wrapper w=factory.createVASTAdWrapper();
             w.setVASTAdTagURI("http://SecondaryAdServer.vast.tag");
             ad.setWrapper(w);
             v.getAd().add(ad);
             w.setVASTAdTagURI("http://www.gmail.com");
             v.getAd().add(ad);
             JAXBContext jc;
        try {
            jc = JAXBContext.newInstance("com.maketag");
            //Crear clasificador
            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            //Clasificar objeto en archivo.
            m.marshal(v, out);
        } catch (Exception ex) {
            Logger.getLogger(VASTTester.class.getName()).log(Level.SEVERE, null, ex);
        }

        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
